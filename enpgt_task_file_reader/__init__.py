import re
from logging import error
from multiprocessing import Pool
from pathlib import Path
from typing import Any, Optional, Union

from tqdm import tqdm

from enpgt_task_file_reader import settings
from enpgt_task_file_reader.polygon_conversion import CoordinateSequence, PolygonData


def get_polygon_from_nsv_coordinate_line(line: str, radius: Optional[int]) -> tuple[str, Optional[PolygonData]]:
    try:
        line_split = re.split("[;,\\t]", line.strip())
        geo_id = line_split[0]
    except Exception as e:
        error(f"Could not interpret line {line.strip()}. Reason: {e}.")
        return "", None

    try:
        coord_seq = CoordinateSequence(line_split[1:], radius)

        return geo_id, coord_seq.get_polygon()
    except Exception as e:
        error(f"Could not create coordinate sequence from line {line.strip()}. Reason: {e}.")
        return geo_id, None


def get_polygon_from_nsv_coordinate_line_star(args: tuple[str, Optional[int]]):
    return get_polygon_from_nsv_coordinate_line(*args)


def multiprocessing_initialize(settings_dict: dict[str, Any]) -> None:
    for setting, old_value in settings_dict.items():
        setattr(settings, setting, old_value)


def convert_nsv_task_file_to_polygons(file_path: Union[Path, str], radius: Optional[int]) -> dict[str, PolygonData]:
    with open(file_path) as f:
        num_lines = sum(1 for _ in f)

    with open(file_path) as file:
        if settings.multiprocessing_cores:
            starmap = [(line.strip(), radius) for line in file.readlines() if line.strip()]

            settings_vals = {setting: getattr(settings, setting) for setting in settings.PASSTHROUGH_SETTINGS}

            if settings.multiprocessing_cores == -1:
                with Pool(
                    initializer=multiprocessing_initialize,
                    initargs=(settings_vals,),
                ) as pool:
                    output = list(
                        tqdm(
                            pool.imap(get_polygon_from_nsv_coordinate_line_star, starmap),
                            total=len(starmap),
                            desc="Parsing task file",
                        )
                    )
            else:
                with Pool(
                    settings.multiprocessing_cores,
                    initializer=multiprocessing_initialize,
                    initargs=(settings_vals,),
                ) as pool:
                    output = list(
                        tqdm(
                            pool.imap(get_polygon_from_nsv_coordinate_line_star, starmap),
                            total=len(starmap),
                            desc="Parsing task file",
                        )
                    )

            return {geo_id: polygon for geo_id, polygon in output if polygon}

        else:
            gen = (
                get_polygon_from_nsv_coordinate_line(line.strip(), radius) for line in file.readlines() if line.strip()
            )
            return {
                geo_id: polygon for geo_id, polygon in tqdm(gen, total=num_lines, desc="Parsing task file") if polygon
            }
