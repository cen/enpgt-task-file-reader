PASSTHROUGH_SETTINGS = ["circle_resolution"]

circle_resolution = 8
# The resolution to use for the circular buffers (in case of radius mode)
# See "quad_segs" in https://shapely.readthedocs.io/en/2.0.6/reference/shapely.buffer.html

multiprocessing_cores = 0
# Enable multiprocessing with as many cores as specified.
# 0 / None = No multiprocessing
# -1 = Use multiprocessing with system default amount of cores
