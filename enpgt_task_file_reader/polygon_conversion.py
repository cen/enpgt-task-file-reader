import re
from dataclasses import dataclass, field
from functools import cached_property
from statistics import mean
from typing import Optional, Union

from pyproj import Proj, Transformer
from shapely import MultiPolygon, Point, Polygon
from shapely import simplify as shapely_simplify
from shapely.geometry import LineString
from shapely.ops import transform as shapely_transform

PROJ_WGS84 = Proj("EPSG:4326")


def str_is_float(f: str) -> bool:
    try:
        float(f)
    except ValueError:
        return False
    return True


@dataclass(frozen=True)
class PolygonData:
    polygon: Union[Polygon, MultiPolygon, Point]
    original_x: Optional[float] = None
    original_y: Optional[float] = None
    radius: Optional[float] = None
    accuracy: Optional[float] = None
    unrecognized_args: dict[str, str] = field(default_factory=dict)

    def __hash__(self):
        return hash(self.polygon) + hash(self.accuracy) + hash(tuple(self.unrecognized_args.items()))

    def __eq__(self, other):
        return (
            self.polygon == other.polygon
            and self.accuracy == other.accuracy
            and self.unrecognized_args == other.unrecognized_args
        )

    @cached_property
    def safe_bounds(self) -> tuple[float, float, float, float]:
        if isinstance(self.polygon, Point):
            return self.polygon.bounds

        longer_side = max(
            self.polygon.bounds[2] - self.polygon.bounds[0], self.polygon.bounds[3] - self.polygon.bounds[1]
        )
        return self.polygon.buffer(longer_side / 10).bounds


@dataclass
class CoordinateSequence:
    coordinate_sequence: list[str]
    radius: Optional[int]

    extra_args: dict[str, str] = field(default_factory=dict)

    def __post_init__(self):
        self.coordinate_sequence = [token.lower() for token in self.coordinate_sequence]
        self.coordinate_sequence, self.extra_args = self.parse_extra_args()

        if not self.coordinate_sequence:
            raise ValueError(f'Coordinate sequence "{self.coordinate_sequence}" is invalid.')
        if len(self.coordinate_sequence) % 2:
            raise ValueError(
                f'Coordinate sequence "{self.coordinate_sequence} has an odd number of elements.'
                f"Sequences are always coordinate pairs + functional pairs such as POLYSTART/POLYEND."
            )

        types = {
            "Single Point": self.is_single_point,
            "Linestring": self.is_linestring,
            "Polygon/Multipolygon": self.is_multipolygon,
        }

        if sum(types.values()) == 0:
            raise ValueError("Coordinate sequence didn't match any known type.")
        if sum(types.values()) > 1:
            raise ValueError(f"Coordinate sequence matched multiple types: {', '.join(k for k,v in types if v)}")

        if self.is_multipolygon and all(str_is_float(token) for token in self.coordinate_sequence):
            self.coordinate_sequence.insert(0, "polystart")
            self.coordinate_sequence.append("polyend")

    def parse_extra_args(self) -> tuple[list[str], dict[str, str]]:
        coordinates = []
        args = {}

        for coordinate in self.coordinate_sequence:
            if re.match(r"\w+=.+", coordinate):
                left, right = coordinate.split("=")
                left, right = left.strip(), right.strip()
                args[left] = right
            else:
                coordinates.append(coordinate)

        return coordinates, args

    @cached_property
    def is_single_point(self) -> bool:
        if not len(self.coordinate_sequence) == 2:
            return False

        if self.radius is None:  # 0 is supported -> will return a point
            raise ValueError("For locations defined as coordinate pairs, a radius must be provided.")

        return True

    @cached_property
    def is_linestring(self) -> bool:
        if len(self.coordinate_sequence) <= 2:
            return False

        if not any(token.startswith("line") for token in self.coordinate_sequence):
            return False

        if not self.coordinate_sequence[0].startswith("line") and self.coordinate_sequence[0].endswith("start"):
            raise ValueError(
                f'Sequence "{self.coordinate_sequence}" includes linestrings,'
                f' but does not start with "linestringstart".'
            )
        if not self.coordinate_sequence[-1].startswith("line") and self.coordinate_sequence[-1].endswith("end"):
            raise ValueError(
                f'Sequence "{self.coordinate_sequence}" includes linestrings,'
                f' but does not start with "linestringend".'
            )

        if not self.radius:
            raise ValueError("For linestring definitions, a radius must be provided.")

        return True

    @cached_property
    def is_multipolygon(self) -> bool:
        if len(self.coordinate_sequence) <= 2:
            return False

        if all(str_is_float(token) for token in self.coordinate_sequence):
            return True

        if not any(token.startswith("poly") for token in self.coordinate_sequence):
            return False

        if not self.coordinate_sequence[0].startswith("poly") and self.coordinate_sequence[0].endswith("start"):
            raise ValueError(
                f'Sequence "{self.coordinate_sequence}" includes linestrings, but does not start with "polystart".'
            )
        if not self.coordinate_sequence[-1].startswith("poly") and self.coordinate_sequence[-1].endswith("end"):
            raise ValueError(
                f'Sequence "{self.coordinate_sequence}" includes linestrings, but does not start with "polyend".'
            )

        return True

    def split_coordinates(self) -> list[list[tuple[float, float]]]:
        individual_sequences = list()
        current_list = list()

        last_was_start = False
        poly_just_ended = True

        for token in self.coordinate_sequence:
            if token.endswith("start"):
                if last_was_start:
                    raise ValueError(f"There are two consecutive polystarts in {self.coordinate_sequence}.")
                last_was_start = True
                poly_just_ended = False
                current_list = list()
                continue
            elif poly_just_ended:
                raise ValueError(f"polyend was not immediately followed by polystart: {self.coordinate_sequence}.")

            if token.endswith("end"):
                if len(current_list) < 6:
                    raise ValueError(f"Polygons must have at least 3 coordinates: {self.coordinate_sequence}")

                last_was_start = False
                poly_just_ended = True
                individual_sequences.append(current_list)
                continue
            else:
                poly_just_ended = False

            try:
                current_list.append(float(token))
            except ValueError:
                raise ValueError(f"Did not recognize token {token} in coordinate list {self.coordinate_sequence}")

        if any(len(individual_sequence) % 2 for individual_sequence in individual_sequences):
            raise ValueError(
                f"Some coordinate sequences have an odd number of elements in coordinate list"
                f" {self.coordinate_sequence}"
            )

        coordinate_sequence_list = [
            list(zip(individual_sequence[1::2], individual_sequence[0::2]))
            for individual_sequence in individual_sequences
        ]

        return coordinate_sequence_list

    def get_polygon(self) -> PolygonData:
        accuracy = float(self.extra_args.pop("accuracy", None))

        if self.is_multipolygon:
            return PolygonData(
                get_polygon_from_multipolygon(self), accuracy=accuracy, unrecognized_args=self.extra_args
            )

        if self.is_linestring:
            return PolygonData(get_polygon_from_line(self), accuracy=accuracy, unrecognized_args=self.extra_args)

        if self.is_single_point:
            return PolygonData(
                get_polygon_from_point(self),
                float(self.coordinate_sequence[1]),
                float(self.coordinate_sequence[0]),
                self.radius,
                accuracy,
                self.extra_args,
            )


def get_polygon_from_line(coordinate_sequence: CoordinateSequence) -> Union[Polygon, MultiPolygon]:
    polygons = []

    for sublist in coordinate_sequence.split_coordinates():
        line_string = LineString(sublist)

        mid_lng = mean(coordinate[0] for coordinate in sublist)
        mid_lat = mean(coordinate[1] for coordinate in sublist)

        aeqd = Proj(proj="aeqd", lon_0=mid_lng, lat_0=mid_lat, datum="WGS84", units="m")

        transproj = Transformer.from_proj(PROJ_WGS84, aeqd, always_xy=True)
        reverse_transproj = Transformer.from_proj(aeqd, PROJ_WGS84, always_xy=True)

        line_string_aeqd = shapely_transform(transproj.transform, line_string)

        polygon_aeqd = line_string_aeqd.buffer(coordinate_sequence.radius, resolution=30)
        polygon_aeqd_simplified = shapely_simplify(polygon_aeqd, min(3.0, coordinate_sequence.radius / 10))

        polygon_wgs84_simplified = shapely_transform(reverse_transproj.transform, polygon_aeqd_simplified)

        polygons.append(polygon_wgs84_simplified)

    if len(polygons) == 1:
        return polygons[0]

    return MultiPolygon(polygons)


def get_polygon_from_point(coordinate_sequence: CoordinateSequence) -> Polygon:
    lng = float(coordinate_sequence.coordinate_sequence[1])
    lat = float(coordinate_sequence.coordinate_sequence[0])

    aeqd = Proj(proj="aeqd", lon_0=lng, lat_0=lat, datum="WGS84", units="m")
    reverse_transproj = Transformer.from_proj(aeqd, PROJ_WGS84, always_xy=True)

    polygon = Point((0, 0))
    if coordinate_sequence.radius != 0:
        polygon = polygon.buffer(coordinate_sequence.radius, resolution=30)

    return shapely_transform(reverse_transproj.transform, polygon)


def get_polygon_from_multipolygon(coordinate_sequence: CoordinateSequence) -> Union[Polygon, MultiPolygon]:
    polygons = [Polygon(sublist) for sublist in coordinate_sequence.split_coordinates()]

    if len(polygons) == 1:
        return polygons[1]

    return MultiPolygon(polygons)
