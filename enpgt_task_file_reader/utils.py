from collections import defaultdict
from collections.abc import Iterable, Sequence
from typing import TypeVar

from enpgt_task_file_reader import PolygonData

T = TypeVar("T")


def reverse_geo_id_to_polygon_data(geo_id_to_polygon_data: dict[str, PolygonData]) -> dict[PolygonData, list[str]]:
    ret = defaultdict(list)
    for geo_id, polygon_data in geo_id_to_polygon_data.items():
        ret[polygon_data].append(geo_id)

    return ret


def match_polygon_data_results_to_geo_ids(
    results_by_polygon_data: dict[PolygonData, T],
    polygon_data_to_geo_ids: dict[PolygonData, Iterable[str]],
    original_geo_id_order: Sequence[str] | dict[str, ...] | None = None,
) -> dict[str, T]:
    ret = dict()

    for polygon_data, result in results_by_polygon_data.items():
        for geo_id in polygon_data_to_geo_ids[polygon_data]:
            ret[geo_id] = result

    if original_geo_id_order is not None:
        assert all(geo_id in original_geo_id_order for geo_id in ret), "Order parameter must contain every geo_id used."
        ret = {geo_id: ret[geo_id] for geo_id in original_geo_id_order}

    return ret
